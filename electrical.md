# LEDs

## integrated driver vs dumb LEDs

integrated
+ small package
+ single device
+ less lines
+ cheap "neopixels"
+ very small sizes
- current draw
- resolution stuff
- potential flicker issues if refresh not high

dumb LEDs
+ current draw
+ small package
- requires led driver
/ basically opposite what I said for integrated

- apparently cost! not surprising, but unfortunate.
+ free leds, lots of salvage means 0 cost, except for driver+boost

## candidates

typical APA or WS or SK neopixels
RGBW ideally
no bypass is ok

5050
    https://www.aliexpress.com/item/32814778563.html?spm=a2g0o.productlist.0.0.2731539e6DGnHr&algo_pvid=20e7eebe-1d87-46a3-ac73-eb3c27c8ac4a&algo_expid=20e7eebe-1d87-46a3-ac73-eb3c27c8ac4a-0&btsid=b8cc95e7-be44-44a8-a19d-8f417d68b8a8&ws_ab_test=searchweb0_0,searchweb201602_10,searchweb201603_55
    $112.6/1000pcs
3535
2020
1515


## plan
revisions are denoted by control/power distribution architectures

### rev1a - socialist
* intLEDs
    * typical APA or WS or SK neopixels
    * RGBW
* architecture
    * socialist architecture (all equal)
    * bridges to connect socialists
* target application: home decor, initial prototype
* target areas: ecad, sw, mcad, manufacturing, cost
    * sw: overreach, trying cheap mcu but not familiar
    * ecad: forgot to include programming pins
    * mcad: neglected to design for mcad; not sure enclosure/mounting strategy
    * manufacturing: mezz connectors are a pita
    * cost: mezz connectors are expensive, psoc mcu is cheapest, but OTP mcu are even cheaper

### rev1b - pylon
* dumbLEDs
* architecture
    * pylon architecture
    * face and edge systems
* target application: Amy's camper
* target areas: ecad, manufacturing, mcad, sw
    * ecad: reworking architecture to lower costs and optimize power
    * mcad: 
    * manufacturing: intending to design for 100-400 micro cells
    * sw: using attiny mcu to focus on ecad, mcad, manufacturing. Not caring about cost as much

### rev2

...

### rev??
* arbitrary electronics
* architecture
    * pylon architecture
    * origami pylon and edge system
    * fabric scaffold
* target application: smart clothes

1515 size leds (smart or dumb)


