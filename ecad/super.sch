EESchema Schematic File Version 4
LIBS:pylon-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR05
U 1 1 5DDCF9AB
P 3350 3350
F 0 "#PWR05" H 3350 3100 50  0001 C CNN
F 1 "GND" H 3355 3177 50  0000 C CNN
F 2 "" H 3350 3350 50  0001 C CNN
F 3 "" H 3350 3350 50  0001 C CNN
	1    3350 3350
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR04
U 1 1 5DDD0266
P 3350 1550
F 0 "#PWR04" H 3350 1400 50  0001 C CNN
F 1 "+5V" H 3365 1723 50  0000 C CNN
F 2 "" H 3350 1550 50  0001 C CNN
F 3 "" H 3350 1550 50  0001 C CNN
	1    3350 1550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5E06ACA2
P 5200 2150
F 0 "C1" H 5315 2196 50  0000 L CNN
F 1 "1u" H 5315 2105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5238 2000 50  0001 C CNN
F 3 "~" H 5200 2150 50  0001 C CNN
	1    5200 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5E06ADE4
P 5500 2150
F 0 "C3" H 5615 2196 50  0000 L CNN
F 1 "0.1u" H 5615 2105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5538 2000 50  0001 C CNN
F 3 "~" H 5500 2150 50  0001 C CNN
	1    5500 2150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR030
U 1 1 5E06C06F
P 5350 1900
F 0 "#PWR030" H 5350 1750 50  0001 C CNN
F 1 "+5V" H 5365 2073 50  0000 C CNN
F 2 "" H 5350 1900 50  0001 C CNN
F 3 "" H 5350 1900 50  0001 C CNN
	1    5350 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 2000 5350 2000
Connection ~ 5350 2000
Wire Wire Line
	5350 2000 5500 2000
Wire Wire Line
	5200 2300 5350 2300
$Comp
L power:GND #PWR031
U 1 1 5E06DC4F
P 5350 2400
F 0 "#PWR031" H 5350 2150 50  0001 C CNN
F 1 "GND" H 5355 2227 50  0000 C CNN
F 2 "" H 5350 2400 50  0001 C CNN
F 3 "" H 5350 2400 50  0001 C CNN
	1    5350 2400
	1    0    0    -1  
$EndComp
Connection ~ 5350 2300
Wire Wire Line
	5350 2300 5500 2300
Wire Wire Line
	5350 2000 5350 1900
Wire Wire Line
	5350 2300 5350 2400
$Comp
L pylon-rescue:AP3012-modLEDs_common U2
U 1 1 5E58DB03
P 2550 4550
F 0 "U2" H 2550 4915 50  0000 C CNN
F 1 "AP3012" H 2550 4824 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 2200 4400 50  0001 C CNN
F 3 "" H 2200 4400 50  0001 C CNN
	1    2550 4550
	1    0    0    -1  
$EndComp
$Comp
L pylon-rescue:BCR420UW6-modLEDs_common U3
U 1 1 5E58E179
P 6200 4250
F 0 "U3" H 6200 4565 50  0000 C CNN
F 1 "BCR420UW6" H 6200 4474 50  0000 C CNN
F 2 "BCR420UW6:SOT95P280X140-6N" H 6200 4250 50  0001 C CNN
F 3 "" H 6200 4250 50  0001 C CNN
	1    6200 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5E59C1DF
P 1750 4850
F 0 "C4" H 1865 4896 50  0000 L CNN
F 1 "1u" H 1865 4805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1788 4700 50  0001 C CNN
F 3 "~" H 1750 4850 50  0001 C CNN
	1    1750 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5E59D154
P 3550 4850
F 0 "C5" H 3665 4896 50  0000 L CNN
F 1 "1u" H 3665 4805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3588 4700 50  0001 C CNN
F 3 "~" H 3550 4850 50  0001 C CNN
	1    3550 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5E59E542
P 3200 4550
F 0 "R1" H 3270 4596 50  0000 L CNN
F 1 "72k" H 3270 4505 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3130 4550 50  0001 C CNN
F 3 "~" H 3200 4550 50  0001 C CNN
	1    3200 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5E59ED2D
P 3200 4850
F 0 "R2" H 3270 4896 50  0000 L CNN
F 1 "5k" H 3270 4805 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3130 4850 50  0001 C CNN
F 3 "~" H 3200 4850 50  0001 C CNN
	1    3200 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:L L1
U 1 1 5E59F497
P 2550 4050
F 0 "L1" V 2740 4050 50  0000 C CNN
F 1 "10uH" V 2649 4050 50  0000 C CNN
F 2 "Inductor_SMD:L_Taiyo-Yuden_MD-3030" H 2550 4050 50  0001 C CNN
F 3 "~" H 2550 4050 50  0001 C CNN
	1    2550 4050
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N5819 D1
U 1 1 5E59FD27
P 3050 4050
F 0 "D1" H 3050 3834 50  0000 C CNN
F 1 "1N5819" H 3050 3925 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 3050 3875 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88525/1n5817.pdf" H 3050 4050 50  0001 C CNN
	1    3050 4050
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5E5ACF08
P 2550 5150
F 0 "#PWR0101" H 2550 4900 50  0001 C CNN
F 1 "GND" V 2550 4900 50  0000 C CNN
F 2 "" H 2550 5150 50  0001 C CNN
F 3 "" H 2550 5150 50  0001 C CNN
	1    2550 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 4700 1750 4700
Wire Wire Line
	2550 4900 2550 5100
Wire Wire Line
	1750 5000 1750 5100
Wire Wire Line
	1750 5100 2550 5100
Connection ~ 2550 5100
Wire Wire Line
	2550 5100 2550 5150
Wire Wire Line
	3200 5100 2550 5100
Wire Wire Line
	3200 5000 3200 5100
Wire Wire Line
	3000 4700 3200 4700
Connection ~ 3200 4700
Wire Wire Line
	2400 4050 2350 4050
Wire Wire Line
	2350 4050 2350 4300
Wire Wire Line
	2700 4050 2750 4050
Wire Wire Line
	2750 4050 2750 4300
Wire Wire Line
	3200 4050 3200 4400
Wire Wire Line
	2750 4050 2900 4050
Connection ~ 2750 4050
Wire Wire Line
	3200 4050 3550 4050
Wire Wire Line
	3550 4050 3550 4700
Connection ~ 3200 4050
Wire Wire Line
	3550 5000 3550 5100
Wire Wire Line
	3550 5100 3200 5100
Connection ~ 3200 5100
Wire Wire Line
	3550 4050 3750 4050
Connection ~ 3550 4050
Wire Wire Line
	2350 4050 1750 4050
Wire Wire Line
	1750 4050 1750 4700
Connection ~ 2350 4050
Connection ~ 1750 4700
Wire Wire Line
	1750 4050 1550 4050
Connection ~ 1750 4050
Text Label 3750 4050 2    50   ~ 0
18V
$Comp
L power:GND #PWR0102
U 1 1 5E5D60CA
P 6200 4650
F 0 "#PWR0102" H 6200 4400 50  0001 C CNN
F 1 "GND" V 6200 4400 50  0000 C CNN
F 2 "" H 6200 4650 50  0001 C CNN
F 3 "" H 6200 4650 50  0001 C CNN
	1    6200 4650
	1    0    0    -1  
$EndComp
Text Notes 4050 4400 0    50   ~ 0
VOUT should be (6V * 3 = 18V)\nVOUT = 1.25* (1+R1/R2)\n18 = 1.25 * (1 + R1/R2)\nR1/R2 = 14.4\n72k/5k = 14.4
Text Label 5850 4200 2    50   ~ 0
18V
$Comp
L Device:R R3
U 1 1 5E5FE7D2
P 6550 4450
F 0 "R3" H 6620 4496 50  0000 L CNN
F 1 "100" H 6620 4405 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6480 4450 50  0001 C CNN
F 3 "~" H 6550 4450 50  0001 C CNN
	1    6550 4450
	1    0    0    -1  
$EndComp
Text Label 6550 4200 0    50   ~ 0
VOUT
Wire Wire Line
	6550 4600 6200 4600
Wire Wire Line
	6200 4600 6200 4650
Wire Wire Line
	5850 4300 5850 4600
Wire Wire Line
	5850 4600 6200 4600
Connection ~ 6200 4600
$Comp
L power:+5V #PWR0103
U 1 1 5E626982
P 1550 4050
F 0 "#PWR0103" H 1550 3900 50  0001 C CNN
F 1 "+5V" H 1565 4223 50  0000 C CNN
F 2 "" H 1550 4050 50  0001 C CNN
F 3 "" H 1550 4050 50  0001 C CNN
	1    1550 4050
	0    -1   -1   0   
$EndComp
$Comp
L pylon-rescue:ATtiny406-M-MCU_Microchip_ATtiny U1
U 1 1 5E6AD4A6
P 3350 2450
F 0 "U1" H 3350 3531 50  0000 C CNN
F 1 "ATtiny406-M" H 3350 3440 50  0000 C CNN
F 2 "Package_DFN_QFN:VQFN-20-1EP_3x3mm_P0.4mm_EP1.7x1.7mm" H 3350 2450 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Microchip%208bit%20mcu%20AVR%20ATtiny406%20data%20sheet%2040001976A.pdf" H 3350 2450 50  0001 C CNN
	1    3350 2450
	1    0    0    -1  
$EndComp
Text Label 3950 2250 0    50   ~ 0
TX_LED4
Text Notes 4550 3200 1    50   ~ 0
need to check physical locations of pins still
Text Notes 6000 2850 1    50   ~ 0
check bypass cap req
$Comp
L power:+5V #PWR0104
U 1 1 5E6E1D91
P 9400 1450
F 0 "#PWR0104" H 9400 1300 50  0001 C CNN
F 1 "+5V" H 9415 1623 50  0000 C CNN
F 2 "" H 9400 1450 50  0001 C CNN
F 3 "" H 9400 1450 50  0001 C CNN
	1    9400 1450
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5E6E4CEB
P 9400 1650
F 0 "#PWR0105" H 9400 1400 50  0001 C CNN
F 1 "GND" H 9405 1477 50  0000 C CNN
F 2 "" H 9400 1650 50  0001 C CNN
F 3 "" H 9400 1650 50  0001 C CNN
	1    9400 1650
	0    -1   1    0   
$EndComp
Text Label 9400 1550 0    50   ~ 0
RX1
Text Label 8700 4450 2    50   ~ 0
TX_LED1
Text Label 8900 1550 2    50   ~ 0
TX_LED1
Text Notes 5050 5250 0    50   ~ 0
change BCR out for the lower rating one (cheaper?)
Text Label 8900 1450 2    50   ~ 0
VOUT
Text Label 8900 1650 2    50   ~ 0
VLED1
Text Notes 3900 4650 0    50   ~ 0
small drop due to fet, and bcr sets vout @ 1.4v\ndo math to accomodate
$Comp
L Device:Q_NMOS_SGD Q1
U 1 1 5E680636
P 8900 4450
F 0 "Q1" H 9104 4496 50  0000 L CNN
F 1 "Q_NMOS_SGD" H 9104 4405 50  0000 L CNN
F 2 "super:SOT-23-reversed" H 9100 4550 50  0001 C CNN
F 3 "~" H 8900 4450 50  0001 C CNN
	1    8900 4450
	1    0    0    -1  
$EndComp
Text Notes 2750 3700 0    50   ~ 0
check diode replace lcsc
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J1
U 1 1 5E6B0B6A
P 9200 1550
F 0 "J1" H 9250 1867 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 9250 1776 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x03_P2.54mm_Vertical" H 9200 1550 50  0001 C CNN
F 3 "~" H 9200 1550 50  0001 C CNN
	1    9200 1550
	-1   0    0    -1  
$EndComp
Text Label 3950 2150 0    50   ~ 0
TX_LED3
Text Label 3950 2050 0    50   ~ 0
TX_LED2
Text Label 3950 2650 0    50   ~ 0
TX_LED1
Text Label 3950 2450 0    50   ~ 0
RX4
Text Label 3950 2350 0    50   ~ 0
RX3
Text Label 3950 1950 0    50   ~ 0
RX2
Text Label 3950 1750 0    50   ~ 0
RX1
Text Label 3950 2950 0    50   ~ 0
RX6
Text Label 3950 2850 0    50   ~ 0
TX_LED6
Text Label 3950 2750 0    50   ~ 0
TX_LED5
Text Label 3950 3050 0    50   ~ 0
RX5
Text Notes 4700 2900 0    50   ~ 0
hw rx, tx on 11, 12
Text Notes 4700 3050 0    50   ~ 0
pwm pins are: 2,5,6,12,13,14
$Comp
L Device:LED D2
U 1 1 5EF933C1
P 9000 3250
F 0 "D2" V 9039 3133 50  0000 R CNN
F 1 "LED" V 8948 3133 50  0000 R CNN
F 2 "AP3012KTR-G1:GW_PSLR31_EM" H 9000 3250 50  0001 C CNN
F 3 "~" H 9000 3250 50  0001 C CNN
	1    9000 3250
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J2
U 1 1 5EF93CE6
P 9200 2300
F 0 "J2" H 9250 2617 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 9250 2526 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x03_P2.54mm_Vertical" H 9200 2300 50  0001 C CNN
F 3 "~" H 9200 2300 50  0001 C CNN
	1    9200 2300
	-1   0    0    -1  
$EndComp
Text Label 8900 2300 2    50   ~ 0
TX_LED1
Text Label 8900 2200 2    50   ~ 0
VOUT
Text Label 8900 2400 2    50   ~ 0
VLED1
$Comp
L power:+5V #PWR0106
U 1 1 5EF961FD
P 9400 2200
F 0 "#PWR0106" H 9400 2050 50  0001 C CNN
F 1 "+5V" H 9415 2373 50  0000 C CNN
F 2 "" H 9400 2200 50  0001 C CNN
F 3 "" H 9400 2200 50  0001 C CNN
	1    9400 2200
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5EF96203
P 9400 2400
F 0 "#PWR0107" H 9400 2150 50  0001 C CNN
F 1 "GND" H 9405 2227 50  0000 C CNN
F 2 "" H 9400 2400 50  0001 C CNN
F 3 "" H 9400 2400 50  0001 C CNN
	1    9400 2400
	0    -1   1    0   
$EndComp
Text Label 9400 2300 0    50   ~ 0
RX1
$Comp
L Device:LED D3
U 1 1 5EF977A5
P 9000 3550
F 0 "D3" V 9039 3433 50  0000 R CNN
F 1 "LED" V 8948 3433 50  0000 R CNN
F 2 "AP3012KTR-G1:GW_PSLR31_EM" H 9000 3550 50  0001 C CNN
F 3 "~" H 9000 3550 50  0001 C CNN
	1    9000 3550
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D4
U 1 1 5EF98513
P 9000 3850
F 0 "D4" V 9039 3733 50  0000 R CNN
F 1 "LED" V 8948 3733 50  0000 R CNN
F 2 "AP3012KTR-G1:GW_PSLR31_EM" H 9000 3850 50  0001 C CNN
F 3 "~" H 9000 3850 50  0001 C CNN
	1    9000 3850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9000 3100 9800 3100
Text Label 9200 3100 2    50   ~ 0
VLED1
Text Label 9500 3100 2    50   ~ 0
VLED2
Text Label 9800 3100 2    50   ~ 0
VLED3
Text Notes 8300 5250 0    50   ~ 0
instead of highside, fet is now lowside\n(on the light board)
Text Notes 8300 1050 0    50   ~ 0
both connectors called out for comprehensiveness
Text Label 9000 4650 3    50   ~ 0
VOUT
Wire Wire Line
	9000 4250 9000 4000
Text Notes 8300 5550 0    50   ~ 0
VOUT = VS - VLED\n(may need to adjust VS to hit max 2.3V Vth trigger)
Text Notes 8300 5700 0    50   ~ 0
NMOS has reversed footprint (matches datasheet now)
$EndSCHEMATC
