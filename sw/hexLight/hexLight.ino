/* Turn on all connected lights to certain brightness using pwm outputs
 * ATtiny406
 */

const int TX_LED1 = PIN_PB0;
const int TX_LED2 = PIN_PA3;
const int TX_LED3 = PIN_PA4;
const int TX_LED4 = PIN_PA5;
const int TX_LED5 = PIN_PB1;
const int TX_LED6 = PIN_PB2;

const int RX1 = PIN_PA0;
const int RX2 = PIN_PA2;
const int RX3 = PIN_PA6;
const int RX4 = PIN_PA7;
const int RX5 = PIN_PB4;
const int RX6 = PIN_PB3;

//float duty = 0.5;
//int brightness = duty * 255;

void setup() {
  // put your setup code here, to run once:
  pinMode(TX_LED1, OUTPUT);
  pinMode(TX_LED2, OUTPUT);
  pinMode(TX_LED3, OUTPUT);
  pinMode(TX_LED4, OUTPUT);
  pinMode(TX_LED5, OUTPUT);
  pinMode(TX_LED6, OUTPUT);

  pinMode(RX1, INPUT);
  pinMode(RX2, INPUT);
  pinMode(RX3, INPUT);
  pinMode(RX4, INPUT);
  pinMode(RX5, INPUT);
  pinMode(RX6, INPUT);

}

void loop() {
  // put your main code here, to run repeatedly:

  delay(1000);
  digitalWrite(TX_LED6, HIGH);

  for(float i=0; i<=1; i += 0.1) {
      //int brightness = i * 255;
      int brightness = 255;

      analogWrite(TX_LED1, brightness);
      analogWrite(TX_LED2, brightness);
      analogWrite(TX_LED3, brightness);
      analogWrite(TX_LED4, brightness);
      analogWrite(TX_LED5, brightness);
      //analogWrite(TX_LED6, brightness);
  }
}
