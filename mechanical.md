# cells

determine min number of tesselating shape types to maximize configurations for organic surfaces
    contoured surfaces will determine shapes to some degree
determine minimum size to comply w/ requried bend radius
    (some examples from supercon in source material)

# LED arrays

example ideas in source material

# interconnects

hinge-like mechanism (1-DOF)
    recommend printing some chainmail to get a feel for how the structure feels/correct geometry
    (technically 2-DOF, but more like 1-DOF w/ loose contraints)
connectors don't necessarily require tooling
    mill could be used instead (Bantam; quoted @ 6mil (150um) resolution)

    rigid toolchanger (jubilee) could be the key to control over the process
        what if: milled components, then pick and place assembly...?
        also, some insertion of fastener (a pin, or bolt)?
        then, (if pin), maybe a soldering iron attachment to "sinch" the placement
        tada, homebrew connector in one machine!

    alternative to injection molding
        epoxy glue
        rubber epoxy
        hotglue gun + metal inset gate
        resin casting

        the prior are all low-heat processes, so low-temp plastic/resin mold is possible
            sla mold
        
        this combo of ideas is potentially useful for ASEP as well

    PCB connector is a potentially low-obstacle idea
        copper-clad fiberglass
        can get really thin

    good contact surface (mate)
        magnet idea 
            curie temp an issue, but maybe not the worst
            hard if get too small

        compliant mechs, either conductor or insulator
            perhaps plating of insulator
            LDS material is easier to obtain these days
                health risks?
                also, requires plating setup.
            what about stranded wire?
            flex, w/ ACA?

# substrate
    PCB is the obvious one
    electrodermis?
        could also be used as interconnect
        durable, meant to flex repeatedly
        conforms to organic shapes
        could easily adhere to another fabric substrate (motherboard)
        use ACA to create "flex to board" connections
            too fragile?
            or too permanent?
        could also back w/ rigid structure
    crochete substrate
        idea from "invisible women"
        woven conductors/insulators
        coaster pattern may provide insight as to how different threads interface w/ each other
        likely difficult to attain resolution requirements/connect to small pads?
            what if daughter card approach?
            PCB sequins, grid mesh for pads so machine sewing is easy
                maybe copper braid? even better than grid

        CNC weave machine, still thinking about it
        also neuralink sewing machine
            use CV to compensate for dynamic substrate (fabric)

# scale
calculated ~6000 cm^2 area for a "shirt"
    60cm * 45cm (rounded up)
cheapest price for 1515 is ~$76.29/1000pcs
    https://www.aliexpress.com/item/32958032852.html
    unit price is $0.07629
the big issue I'm seeing currently is how cost scales:
    hex3 is ~1 cm^2, and uses 19 sk6812 1515 LEDs
        cheapest price for 1515 is ~$76.29/1000pcs
            https://www.aliexpress.com/item/32958032852.html
        so...
        6000cm^2 * 19pcs/1cm^2 * $76.29/1000pcs = $8697.06
            prohibitively expensive
    tri4 is ~4 cm^2 per 10*6 = 60 1515 LEDs
        6000cm^2 * 60/4cm^2 * $76.29/1000pcs = $6866.1

    wondering if we can do something like "adaptive layer heights"...
        use different sized LEDs on the same cell to lower costs, but maintain feeling of density
        enables "sub-pixel" resolution still

        thinking that the combo of LEDs/cell-types/size of cells will all be determined by analysis of mesh based on surface I want to cover
            eg, using something like pepakura to unfold a scan of a model of my body
    
    

watch band (easier)
    measured ~12cm long band
    tri3 is ~1cm * width (3 triangles long), 3*tri3/18pcs = 18pcs/cm
    12cm * 18pcs/cm * $76.29/1000pcs = 16.47864
        now THAT's doable

