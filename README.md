# overview

<img src='/imgs/pylon.png' width=600px>
<img src='/imgs/light.png' width=600px>
<img src='/imgs/macro_f.JPG' width=600px>
<img src='/imgs/macro_close.png' width=600px>

the intent of this project is to explore digital fabrication, modular designs, and other essential aspects of project management and engineering design.

non-exaustive list of everything I intend to explore:
* cad library organization
* higher volume manufacturing
* "viral bootloading"
* distributed power systems
* novel interconnect strategies
* soft/compliant substrates
* design tools
* geometry
* wearables

# goals
## months
modular led systems for home decor, car decor, grow lights, polyhedra, wearables, etc.

## 1 year
supa cool modular led suit.
