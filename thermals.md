# voltage @ 17V for 3 duris s 5 leds, full duty cycle
deg(C):current(mA):series_resistance(ohm)
22:10:150   acceptable brightness; doesn't hurt eyes w/ diffuser
26:15:82    acceptable brightness; kinda tough to stare at w/ diffuser
30:20:56    very bright; hurts eyes even w/ diffuser

# voltage @ 17.7V for 3 duris s 5 leds, full duty cycle
28:14:150   acceptable brightness; kinda tough to stare at w/ diffuser
