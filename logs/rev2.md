# communication

## hex pylon 8-soic mcu

hex pylon is intended to control 6 separate led strings, but 8-soic mcu only has 6 gpio.
charlieplexing allows me to reduce gpio req down from 6 to 3 pins.
rev1 socialist architecture (tri) used a single clock line (sync) and 3 separate data lines per side, to communicate w/ neighbors.
obviously, using 6 gpio to communicate w/ neighbors doesn't work for an SO-8 (not even including a clock line). 

doing some thinking on this, seems like we would run into a lot of complexity for sw if we try to use a single UART bus for 6 separate slaves...maybe we could figure it out later, but would be a huge impediment right now. Let's aim to provide dedicated UART per side/client.

maths:
* (12) 6 sides = 6\*2 = 12 gpio
* (1) sync pin (not necessary for communication but synchronized led firing)
* (2) vcc, gnd
* (6) 6 led strings

total: 12+1+2+6 = 21 pin mcu

ouch. sync is not a confirmed issue, maybe get rid of it.
also, tx is not necessary unless new code/command is propagating, so maybe merge tx w/ led pins. rx should always be listening, just in case new code/command comes in.

new maths:
* (6) 6 rx gpio
* (1) sync
* (2) vcc, gnd
* (6) 6 led/tx

total: 6+1+2+6 = 15 pin mcu
w/o sync: 14 pin mcu

given this communication strategy, algo to determine req pins is:
* n: sides
* 2 + 1 + n\*2
* @ pins = 8 w/ sync, 3 + n\*2, n = 2.5
* @ pins = 8 w/o sync, n = 3

wait, forgot that I'm doing charlieplexing. only need 3, not 6 pins for the thing
regardless, if we can get away w/ not charlieplexing, we save having to install 3 resistors. and the mcu has 6 pins req to tx anyways, so we can just do a 1 to 1 thing

---

so I keep switching MCUs, currently have my eyes set on the attiny 406/416's, but apparently not all attiny MCUs are totally supported by arduino. Need to check what is before I commit to design.

# led driving

## control strategy
for 6 led strings:
* charlieplexing required (3pins = 6leds)
* multiplexing requires extra hardware; though consider multiplexing light cells to keep charlieplexing complexity down
* charlieplexing will work for large display, since decentralized driving

# interconnects

for simplicity and cost reasons, we'll likely use header pins
2.54mm if we can't afford stuff, otherwise 1.00mm

# pwm
https://www.youtube.com/watch?v=5SQt1f4PsRU
according to Mike Harrison, 250Hz is a good pwm freq to drive LEDs but avoid audible artefacts
and flickering
12-bits resolution for smooth, linear scale


