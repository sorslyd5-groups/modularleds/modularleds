tri3 mk1
testing for the following:
* brightness at different current draws
* light diffuser end result
* size & resolution combo sufficient for interesting application?
* how many needed for something interesting?
* current combo of leds and diffuser sufficient for req color temp range?

mkx
testing for following:
* how to better distribute power delivery?
    * power backbone connector
    * also 4-layer board to create power planes (and not clutter up already messy routing)
    * use caps to lower current req of interconnects, traces
* how to supply power to the panel assembly itself?
* different substrate options

