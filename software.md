#v0.1

* code turns on lights. the end.

#v0.2

* communication possible (not code propagation)
* with hmi tile connected, can change modes

#v0.3

* capability to put device to sleep w/ interrupt
* turn off peripherals

#v0.4

* capacitive touch

#v0.x

* ???

#v1.0

* code can propagate to neighbors in arbitrary 2d map


#communication protocols

* synchronous protocol (data, sync...should be clk probably)
    + relative latency is solved
    + likely good for master architecture?
* may take a note from Zach and go async (tx, rx)
    + full-duplex potentially
    + potentially good for master-less architecture
    + might be programming pins (virulent bootloader, etc.)
